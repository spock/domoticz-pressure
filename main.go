package main

import (
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/golang/glog"
	"github.com/kidoman/embd"
	_ "github.com/kidoman/embd/host/chip"
	"github.com/kidoman/embd/sensor/bmp180"
	"log"
	"os"
	"time"
)

var celsius string = "\x2103"
var cfgpath string = ""

type domoticzInfo struct {
	Idx             int
	PressureScaling int
	SensorName      string
}

type mqttInfo struct {
	ServerURI      string
	ClientId       string
	Username       string
	Password       string
	PublishTopic   string
	SubscribeTopic string
}

//func (m *mqttInfo) String() string {
//	return fmt.Sprintf("{\"server_uri\": m.ServerURI}")
//}

type Config struct {
	I2CID              byte
	RaportingFrequency int
	Domoticz           domoticzInfo
	MQTT               mqttInfo
}

func init() {
	flag.StringVar(&cfgpath, "config", "/etc/bmp180.conf", "config file path")
	flag.Parse()
}
func main() {
	msgQueue := make(chan string, 2)
	glog.V(2).Infoln("Config path:", cfgpath)
	var conf Config

	if _, err := toml.DecodeFile(cfgpath, &conf); err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}
	go initMQTTpublisher(&conf.MQTT, msgQueue)
	bus := embd.NewI2CBus(conf.I2CID)
	sensor := bmp180.New(bus)
	c := time.Tick(time.Duration(conf.RaportingFrequency) * time.Second)
	for _ = range c {
		// measure temp so Pressure value will have proper corrections
		if _, err := sensor.Temperature(); err != nil {
			log.Fatal(err)
			continue
		}
		pressure, err := sensor.Pressure()
		if err != nil {
			log.Fatal(err)
		}
		hPaPressure := float32(pressure) / float32(conf.Domoticz.PressureScaling)
		msg := fmt.Sprintf("{ \"idx\": %d, \"svalue\": \"%.0f;5\", \"name\": \"%s\" }", conf.Domoticz.Idx, hPaPressure, conf.Domoticz.SensorName)
		glog.V(2).Infoln("Sendind msg to goroutine")
		msgQueue <- msg
		fmt.Println(msg)
	}

}

func initMQTTpublisher(cfg *mqttInfo, msg chan string) {
	opts := mqtt.NewClientOptions().AddBroker(cfg.ServerURI)
	opts.SetClientID("domoticz-pressure-sensor")
	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	for {
		select {
		case text := <-msg:
			glog.V(2).Infoln("Publishing msg")
			token := c.Publish(cfg.PublishTopic, 0, false, text)
			glog.V(2).Infoln("Waiting for token")
			token.Wait()
			glog.V(2).Infoln("Token wain ended")
		}
	}

}
